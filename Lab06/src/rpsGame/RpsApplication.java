/**
 * @author Jei wen Wu
 */

package rpsGame;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	
	private RpsGame rpsGame = new RpsGame();
	
	public void start(Stage stage) {
		Group root = new Group();
		
		//create components
		Button rockButton = new Button("rock");
		Button scissorsButton = new Button("scissors");
		Button paperButton = new Button("paper");
		TextField welcome = new TextField("Welcome!");
		TextField wins = new TextField("wins: 0");
		TextField losses = new TextField("losses: 0");
		TextField ties = new TextField("ties: 0");
		
		//creating boxes
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		VBox overall = new VBox();
		
		//Changing the width of the textFields to 200px
		welcome.setPrefWidth(375);
		wins.setPrefWidth(200);
		losses.setPrefWidth(200);
		ties.setPrefWidth(200);
		
		//Adding the buttons and textFields into their respective HBox
		buttons.getChildren().addAll(rockButton, scissorsButton, paperButton);
		textFields.getChildren().addAll(welcome, wins, losses, ties);
		
		//Creating a VBox and adding both HBox into it
		overall.getChildren().addAll(buttons, textFields);
		
		//adding components to the group
		root.getChildren().addAll(overall);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 1000, 300);
		scene.setFill(Color.BLACK);
		
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		
		stage.show();
		
		//button event handlers
		RpsChoice rock = new RpsChoice(welcome, wins, losses, ties, "rock", rpsGame);
		rockButton.setOnAction(rock);
		RpsChoice scissors = new RpsChoice(welcome, wins, losses, ties, "scissors", rpsGame);
		scissorsButton.setOnAction(scissors);
		RpsChoice paper = new RpsChoice(welcome, wins, losses, ties, "paper", rpsGame);
		paperButton.setOnAction(paper);
		
	}
	
	//main method to run the application (GUI)
    public static void main(String[] args) {
        Application.launch(args);
    }
}

