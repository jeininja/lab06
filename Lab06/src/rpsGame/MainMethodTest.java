/**
 * @author Jei wen Wu
 * This class is used to test the RpsGame class, not a JUnit test but actually running the game
 */

package rpsGame;

import java.util.Scanner;

public class MainMethodTest {

	public static void main (String[] args) {
		Scanner reader = new Scanner(System.in);
		RpsGame newGame = new RpsGame();
		int totalNumOfGames = 3;
		for (int i=0; i<totalNumOfGames; i++) {
			System.out.println("Pick between [rock, paper, scissors]");
			String playerChoice = reader.nextLine();
			String result = newGame.playRound(playerChoice);
			System.out.println(result);
			System.out.println("Score: "+newGame.getWins()+" wins, "+newGame.getTies()+" ties, "+newGame.getLosses()+" losses.");
		}
		
		String winner = "";
		
		if(newGame.getTies()>newGame.getLosses() && newGame.getTies()>newGame.getLosses()) {
			winner = "It was a tie";
		}
		else if(newGame.getWins()<newGame.getLosses()) {
			winner = "The computer won";
		}
		else {
			winner = "You won";
		}
		System.out.println("The match has ended. "+winner);
	}
}